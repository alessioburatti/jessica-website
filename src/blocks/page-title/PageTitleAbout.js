import React from 'react';
import { useTranslation } from 'react-i18next';

const PageTitleAbout = () => {
    const { t } = useTranslation();
    return (
        <section id="page-title">
            <div className="wrapper">
                <div className="title position-relative">
                    <h1>{t('about.title')}<span className="dot">.</span></h1>

                    <div className="title-clone">{t('about.title')}.</div>
                </div>
            </div>
        </section>
    );
};

export default PageTitleAbout;
