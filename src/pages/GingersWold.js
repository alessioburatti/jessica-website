import React, { Fragment } from 'react';
import MetaTags from 'react-meta-tags';

import Loading from '../blocks/loading/Loading';
import Header from '../blocks/header/Header';
import Footer from '../blocks/footer/Footer';
import { useTranslation } from 'react-i18next';

const GingersWorld = () => {
    document.body.classList.add('single');
    document.body.classList.add('single-portfolio');
    document.body.classList.add('bg-fixed');
    document.body.classList.add('bg-line');
    const { t } = useTranslation();
    return (
        <Fragment>
            <MetaTags>
                <meta charSet="UTF-8" />
                <title>Ginger's World | Jessica Mondini</title>

                <meta httpEquiv="x-ua-compatible" content="ie=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="description" content="" />
                <meta name="keywords" content="" />
                <meta name="robots" content="index, follow, noodp" />
                <meta name="googlebot" content="index, follow" />
                <meta name="google" content="notranslate" />
                <meta name="format-detection" content="telephone=no" />
            </MetaTags>

            <Loading />

            <Header />

            <main id="main" className="site-main bg-half-ring-right bg-half-ring-top">
                <section id="page-content">
                    <div className="wrapper">
                        <div id="single">
                            <div className="row gutter-width-lg single-content">
                                <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12">



                                    <div className="img object-fit">
                                        <div className="object-fit-cover">
                                            <img src="assets/img/MockupSito.jpg" alt="General Mockup Website" />
                                        </div>
                                    </div>

                                    <div className="img object-fit">
                                        <div className="object-fit-cover">
                                            <img src="assets/img/GingersDesktop.jpg" alt="Website Desktop" />
                                        </div>
                                    </div>

                                    <div className="img object-fit">
                                        <div className="object-fit-cover">
                                            <img src="assets/img/GingersMobile.jpg" alt="Website Mobile" />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                    <p className="date">
                                        <span className="large">25</span> {t('ginger.may')}, 2020
                                    </p>

                                    <h1 className="small">{t('ginger.name')}</h1>

                                    <div className="description">
                                        <p>{t('ginger.name')}</p>
                                        <h5>Idea</h5>

                                        <p>
                                            • {t('ginger.ideas.0')} <br />
                                            • {t('ginger.ideas.1')} <br />
                                            • {t('ginger.ideas.2')} <br />
                                            • {t('ginger.ideas.3')} <br />
                                            • {t('ginger.ideas.4')} <br />
                                            • {t('ginger.ideas.5')} <br />
                                            • {t('ginger.ideas.6')}
                                        </p>

                                        <h5>InVision Link</h5>

                                        <ul>

                                            <li> <a title="Desktop Link" className="btn btn-link border-0 p-0 transform-scale-h" href="https://gingersworld.invisionapp.com/public/share/ME14FWNU8C (https://gingersworld.invisionapp.com/public/share/ME14FWNU8C)"> Desktop Link</a> </li>

                                            <li> <a title="Mobile Link" className="btn btn-link border-0 p-0 transform-scale-h" href="https://gingersworld.invisionapp.com/public/share/VZ14FRYNF2#screens/477585591 (https://gingersworld.invisionapp.com/public/share/VZ14FRYNF2#screens/477585591)">Mobile link</a> </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </Fragment>
    );
};

export default GingersWorld;
