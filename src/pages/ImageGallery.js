import React, { Component } from 'react';
import Isotope from 'isotope-layout';
import ImagesLoaded from 'imagesloaded';
import PropTypes from 'prop-types';
import GalleryItemsData from '../../data/gallery/galleryItems';

const GalleryItemsData = [
    {
        
    }
]

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.onFilterChange = this.onFilterChange.bind(this);

        this.state = {
            selected: 0
        };
    }

    handleClick(i, e) {
        e.preventDefault();

        this.setState({
            selected: i
        });

        return false
    }

    onFilterChange = (newFilter) => {
        var gallery_items_name = this.grid;
        var gallery_item_name = '.gallery-item';

        if (this.iso === undefined) {
            this.iso = new Isotope(gallery_items_name, {
                itemSelector: gallery_item_name,
                masonry: {
                    horizontalOrder: true
                }
            });
        }

        if (newFilter === '*') {
            this.iso.arrange({ filter: `*` });

        } else {
            this.iso.arrange({ filter: `.${newFilter}` });
        }
    };

    componentDidMount() {
        var gallery_items_name = this.grid;
        var gallery_item_name = '.gallery-item';

        var iso = new Isotope(gallery_items_name, {
            itemSelector: gallery_item_name,
            masonry: {
                horizontalOrder: true
            }
        });

        var imgLoad = new ImagesLoaded(gallery_items_name);

        imgLoad.on('progress', function (instance, image) {
            iso.layout();
        });
    }

    render() {
        return (
            <div className="gallery">
                <div className={"gallery-item-wrapper" + this.props.paddingBottomClass} >
                    <div className="gallery-items" ref={(c) => this.grid = c}>
                        {GalleryItemsData && GalleryItemsData.map((item, key) => {
                            return (
                                <>
                                    {
                                            <a key={key} title={item.title} className={"gallery-item active " + item.category} href={process.env.PUBLIC_URL + item.link}>
                                                <div className="img object-fit">
                                                    <div className="object-fit-cover">
                                                        <img
                                                            style={
                                                                {
                                                                    objectFit: item.scale ? item.scale : ""
                                                                }
                                                            }
                                                            src={item.imgLink}
                                                            alt={item.title} />
                                                    </div>
                                                </div>

                                                <div className="gallery-hover">
                                                    <div className="gallery-hover-wrapper">
                                                        <h3>{item.title}</h3>

                                                        <span className="btn btn-link border-0 transform-scale-h p-0">
                                                            {item.button}
                                                            <i className="icon-c icon-arrow-right" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </a> 
                                    }
                                </>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

Gallery.propTypes = {
    paddingBottomClass: PropTypes.string
};

export default Gallery;
