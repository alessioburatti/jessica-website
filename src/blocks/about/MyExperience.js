import React from 'react';
import { useTranslation } from 'react-i18next';
import ExperienceItemsData from '../../data/experience/experience';

const MyExperience = () => {
    const { t } = useTranslation();
    return (
        <div id="my-experience" className="block spacer p-top-xl">
            <h2>{t('about.my')} <span className="line">{t('about.experience')}.</span></h2>

            <div className="row gutter-width-lg with-pb-lg">
                { ExperienceItemsData && ExperienceItemsData.map( ( item, key ) => {
                    return (
                        <div key={ key } className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card border-0">
                                <div className="card-body p-0">
                                    <h4>{ t(`experiences.${key}.title`) }</h4>

                                    <p className="p-small text-secondary bold">{ t(`experiences.${key}.date`) }</p>

                                    <p>{ t(`experiences.${key}.description`) }</p>
                                </div>
                            </div>
                        </div>
                    );
                } ) }
            </div>
        </div>
    );
};

export default MyExperience;
