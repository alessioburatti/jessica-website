import React from 'react';

const PageTitlePictures = () => {
    return (
        <section id="page-title">
            <div className="wrapper">
                <div className="title position-relative">
                    <h1>My Pictures<span className="dot">.</span></h1>

                    <div className="title-clone">My Pictures.</div>
                </div>
            </div>
        </section>
    );
};

export default PageTitlePictures;
