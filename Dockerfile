FROM nginx:1.19.1-alpine

# Copy build artifacts
RUN mkdir -p /site
COPY /build /site

# Copy Caddyfile
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]