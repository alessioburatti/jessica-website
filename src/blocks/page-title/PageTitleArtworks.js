import React from 'react';

const PageTitleArtworks = () => {
    return (
        <section id="page-title">
            <div className="wrapper">
                <div className="title position-relative">
                    <h1>Artworks<span className="dot">.</span></h1>

                    <div className="title-clone">Artworks.</div>
                </div>
            </div>
        </section>
    );
};

export default PageTitleArtworks;
