import React from 'react';

const PageTitleColorDesign = () => {
    return (
        <section id="page-title">
            <div className="wrapper">
                <div className="title position-relative">
                    <h1>Color Design<span className="dot">.</span></h1>

                    <div className="title-clone">Color Design.</div>
                </div>
            </div>
        </section>
    );
};

export default PageTitleColorDesign;
