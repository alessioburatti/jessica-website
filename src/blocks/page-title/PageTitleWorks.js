import React from 'react';
import { useTranslation } from 'react-i18next';

const PageTitleWorks = () => {
    const { t } = useTranslation()
    return (
        <section id="page-title">
            <div className="wrapper">
                <div className="title position-relative">
                    <h1>{t('works.works')}<span className="dot">.</span></h1>

                    <div className="title-clone">{t('works.works')}.</div>
                </div>
            </div>
        </section>
    );
};

export default PageTitleWorks;
